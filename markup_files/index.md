![Bild 1](media/1.jpeg)

**Projektledaren har mycket att vinna på att arbeta BIM-inriktat och informationsmodellen är till stor nytta vid till exempel samordningsmöten.**

# Arbete med BIM ger stor nytta för projektledaren

> ##### Åtskilliga projektledare ser inte någon direkt koppling mellan BIM och projektledning. Men BIM är högsta grad en projektledningsfråga och för att få ut maximalt av BIM-arbetet och bättre nå projektmålen måste projektledningen vara delaktig i det arbetet. Med detta som bakgrund arbetar en grupp inom BIM Alliance projektledningsgrupp med att ta fram ett dokument som ska fungera som handledning och guide åt projektledare som vill arbeta med BIM.

NÄR MAN PRATAR OM NYTTAN MED BIM är det lätt att förenkla och lyfta fram fördelarna för entreprenörer och projektörer. Men att det krävs en projektledning, som är engagerad och delaktig i BIM-frågorna, för att nyttorna ska uppnås, pratas det väldigt lite om. Nyttan för projektledaren själv nämns om möjligt än mer sällan. Detta vill en nybildad arbetsgrupp råda bot på. 
​	– Vi vill samla in erfarenhet och kunskap och klargöra vilka problem vi står inför, säger Linnea Carlsson, projektledare på Tyréns och en av deltagarna i arbetsgruppen. Branschen och vi projektledare tillsammans med BIM-strateger måste lära av varandra. Genom arbetet i BIM Alliance projektledningsgrupp, som dels syftar till att förenkla processen att komma igång och dels ta fram förslag på gemensamma begrepp, hoppas vi på att fler vill börja arbeta med BIM. 
​	BIM Alliance projektledningsgrupp startade hösten 2012 och består av ett trettiotal deltagare, en blandning av personer som jobbar som projektledare och projekteringsledare men även som BIM-strateger och BIM-samordnare. Gruppen representerar på ett brett plan både byggbranschen och byggprocessen med både byggherrar, konsulter och entreprenörer. Tidigare har en arbetsgrupp inom denna projektledningsgrupp fokuserat på de olika roller som är kopplade till BIMarbetet och konkretiserat vad de olika rollerna ska omfatta (se infobladet ”Tydliga rollbeskrivningar underlättar BIM-arbetet”). Infobladet har redan kommit till nytta vid upphandling av BIMtjänster, exempelvis för ett projekt som Exploateringskontoret i Stockholm leder.
​	Den nya arbetsgruppen, som bildades under slutet av 2014, består förutom av Linnea Carlsson av Nina Borgström, Chef Digital Design och BIM på White, Johanna Bodsten, Teknikstrateg VDC/BIM på Sweco, Daniel Norberg, Projekteringsledare på Peab samt Cecilia Sundberg, Avdelningschef Projektledning Fastighet & Bygg på Tyréns. 
​	– Vår målgrupp är de projektledare som är lite osäkra och inte riktigt vet vad man ska tänka på som projektledare i ett BIM-projekt, säger Linnea Carlsson. Fördelarna med BIM kopplas oftast framför allt till projektet självt och inte till projektledaren. Men denna har mycket att vinna på att arbeta BIM-inriktat. Det blir till exempel lättare att få en gemensam och tydlig visuell målbild som är samma för alla. 

En stor del av projektledarens uppgift är att underlätta kommunikationen mellan de olika parterna och engagera projektets medlemmar – ju tydligare man kan se framsteg desto troligare att projektet får engagerade projektmedlemmar. BIM ger verktyg som underlättar samarbetet, förståelsen och respekten mellan olika discipliner och som minskar risken att man pratar förbi varandra – en huvuduppgift för projektledaren som ska uppmuntra samarbete och hantera konflikter på ett smidigt sätt. Projektledaren ska leda i frågor som rör många och där samsyn måste uppnås.
​	Som projektledare vill man vara en bra rådgivare åt sin beställare.
Ett BIM-baserat arbete gör det lättare att ta fram ett utökat beslutsunderlag, till exempel olika förslag på material och utformningar, och kunna koppla dessa till budget och tidplan. Då kan man på ett tydligt sätt visa hur olika alternativ ser
ut och hur olika val påverkar projektet i stort.
​	Beslutsunderlaget blir rikare och bättre och kan vara avgörande för om beställaren i slutändan får den produkt som efterfrågats. På så vis kan ett BIM-baserat arbete underlätta säkerställandet av den önskade funktionen. Detta kan exempelvis
vara akustiska simuleringar i en aula där olika väggklädnader testas mot varandra.

OVANSTÅENDE SCENARIO FÖRUTSÄTTER att BIM är bestämt från
början – det är viktigt att tidigt veta vad man vill ha ut av att jobba BIM-baserat i ett projekt, vilka BIM-nyttor man vill åt, för att kunna säkerställa rätt nivå för varje enskilt skede. Om det inte från början är klargjort vad man vill kunna göra och vilka arbetssätt och processer projektet vill nyttja, så löper man stor risk att inte kunna göra det man vill när man väl befinner sig i skedet då nyttan önskas.
​	Om kravställningen på projekterande konsulter är för låg,
kan det leda till att de informationsmodeller som tas fram inte håller rätt nivå. Genom att ta några enkla beslut i tidiga skeden av planering undviks detta och projektledaren kan säkerställa ett lyckat BIM-projekt.
​	– Detta är lite klurigt. Därför är det lämpligt att tidigt i projektet jobba med en BIM-strateg, säger Linnea Carlsson. 
​	En projektledare fattar många beslut på egen hand på en nivå där beställare eller projektchef ej är inblandade. Möjligheten att genom ett BIM-baserat arbete kunna ta fram ett gedignare beslutsunderlag blir även en hjälp i det egna beslutsfattandet.
​	– En stor fördel med BIM handlar om den egna förståelsen, att själv kunna jobba proaktivt och vara förutseende kring var vi behöver sätta oss ner och lösa något gränsöverskridande. Med hjälp av samordningsmodellen, som är en sammanställning av alla teknikområdens informationsmodeller, kan vi se problem i god tid och leda olika parter så att vi undviker dessa. Arbetssituationen blir helt enkelt bättre. En projektledare behöver bred kunskap inom många områden, men kan aldrig ha
samma kompetens som projektets specialister. Ett verktyg för att öka den egna förståelsen är därför oerhört värdefullt.

UNDER BYGGFASEN KAN PROJEKTLEDAREN med hjälp av BIM lättare se i vilken ordning saker ska göras och enklare planera produktionsskeden. För att optimera byggprocessen kanske man behöver flytta på etableringar eller göra vissa arbetsmoment i en annan ordning än den förväntade. Med hjälp av samordningsmodellen kan logistiken förbättras liksom framförhållning i produktionen. 
​	BIM kan även möjliggöra ett bättre förfrågningsunderlag, som i sin tur gör det enklare att utvärdera anbud. Det innebär att man kan få bättre upphandling och bättre koll på vilka eventuella problem som entreprenören kan komma att ställas inför. Det skulle även kunna vara möjligt att genom BIM-modellen kravställa att entreprenören ska redovisa produktionens framfart och på så vis få bättre kontroll över byggets framfart.
​	– Man har kontinuerliga möten även under byggfasen liksom under den mycket viktiga kvalitetsgranskningen – byggs det som tänkt? Samordningsmodellen ger utmärkta möjligheter att enkelt kunna göra kvalitetsgranskningar under byggprocessens gång. Vissa delar är kanske inte färdigprojekterade och då kan entreprenören visa mig som projektledare olika förslag. Projektledaren måste även hantera den tredje parten, allmänhet och myndigheter, när det gäller tillstånd och information under byggfasen och även då har man nytta av att projekterande konsulter tar fram informationsmodellen, säger Linnea Carlsson.

UNDER VÅREN 2015 SKA ARBETSGRUPPEN vara klar med sitt arbete som ska resultera i en slags guide för hur projektledaren, projekteringsledaren,  produktionsledaren samt installationssamordnaren kan arbeta. Guiden är tänkt som ett komplement till ”Leda byggprojekt” som är utgivet av Svenska Teknik & Designföretagen, STD, och Svensk Byggtjänst.




